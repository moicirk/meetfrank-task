export default {
  token (state) { return state.token },
  isJoined (state) {
    return !!state.token
  },
  user (state) { return state.user },
  users (state) { return state.users },
  chat (state) { return state.chat }
}
