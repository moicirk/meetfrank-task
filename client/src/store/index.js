import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import { SESSION_TOKEN } from './constants'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem(SESSION_TOKEN),
    user: null,
    users: [],
    chat: []
  },
  actions,
  mutations,
  getters
})
