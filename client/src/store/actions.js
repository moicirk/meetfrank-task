import { SET_USER_DATA, UNSET_USER_DATA, SET_USERS_LIST, SET_CHAT, ADD_MESSAGE } from './mutations'
import apiClient from '@/libs/api-client'

export default {
  async loadUser ({ commit }) {
    const response = await apiClient.get('/users/me')

    commit(SET_USER_DATA, response.data)
  },
  async joinUser ({ commit }, username) {
    const response = await apiClient.post('/users', {
      username
    })

    const { user } = response.data
    commit(SET_USER_DATA, user)

    return user
  },
  async logoutUser ({ commit }) {
    commit(UNSET_USER_DATA)
  },
  async loadUsers ({ commit }) {
    const response = await apiClient.get('/users')

    commit(SET_USERS_LIST, response.data)
  },
  async loadChat ({ commit }, user) {
    const response = await apiClient.get(`/chats/${user}`)

    commit(SET_CHAT, response.data)
  },
  async sendMessage ({ commit }, { user, text }) {
    const response = await apiClient.post(`/chats/${user}`, {
      text
    })

    commit(ADD_MESSAGE, response.data)

    return response.data
  }
}
