import { SESSION_TOKEN } from './constants'

export const SET_USER_DATA = 'SET_USER_DATA'
export const UNSET_USER_DATA = 'UNSET_USER_DATA'
export const SET_USERS_LIST = 'SET_USERS_LIST'
export const SET_CHAT = 'SET_CHAT'
export const ADD_MESSAGE = 'ADD_MESSAGE'

export default {
  [SET_USER_DATA] (state, userData) {
    state.user = {
      ...(state.user || {}),
      ...userData
    }

    const { _id } = userData
    localStorage.setItem(SESSION_TOKEN, _id)
    state.token = _id
  },
  [UNSET_USER_DATA] (state) {
    localStorage.removeItem(SESSION_TOKEN)

    state.user = null
    state.token = null
  },
  [SET_USERS_LIST] (state, users) {
    state.users = users
  },
  [SET_CHAT] (state, chat) {
    state.chat = chat
  },
  [ADD_MESSAGE] (state, message) {
    state.chat.push(message)
  }
}
