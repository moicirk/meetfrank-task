import axios from 'axios'
import store from '@/store'

const client = axios.create({
  baseURL: `${ process.env.VUE_APP_SERVER_URL }/api`
})

client.interceptors.request.use( (config) => {
  const token = store.getters.token
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }

  return config
})

export default client
