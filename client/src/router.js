import VueRouter from 'vue-router'
import store from '@/store'

const requireAuth = async (to, from, next) => {
  if (store.getters.isJoined) {
    if (!store.getters.user) {
      await store.dispatch('loadUser')
    }

    return next()
  }

  if (to.name === 'join') { return next() }

  next('/join')
}

const routes = [
  {
    name: 'home',
    path: '/',
    component: () => import('@/views/ChatsListPage'),
    beforeEnter: requireAuth
  },
  {
    name: 'join',
    path: '/join',
    component: () => import('@/views/JoinPage'),
    beforeEnter: requireAuth
  },
  {
    name: 'chat',
    path: '/chat/:user',
    component: () => import('@/views/ChatPage'),
    beforeEnter: requireAuth
  }
]

export default new VueRouter({
  mode: 'history',
  routes
})
