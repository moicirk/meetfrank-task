import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/main.css'
import Vue from 'vue'
import VueRouter from 'vue-router'
import router from '@/router'
import store from '@/store/index'
import VueSocketIO from 'vue-socket.io'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(new VueSocketIO({
  debug: true,
  connection: process.env.VUE_APP_SERVER_URL
}))

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
