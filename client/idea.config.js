/**
 * This file help JetBrains to resolve @ alias via autocomplete.
 * Set it as a webpack config in preferences
 */

const path = require('path')

module.exports = {
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src')
    }
  }
}
