const path = require('path')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const api = require('./api')

const app = express()

app.use(morgan('tiny'))
app.use(cors())
app.use(bodyParser.json())
app.use(express.static('static'))
app.use('/api', api)

app.get('*', (req, res) => {
  res.sendFile(path.resolve(global.root, 'static/index.html'))
})

module.exports = app
