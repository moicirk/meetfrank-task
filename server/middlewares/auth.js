const {User} = require('../models')

const getTokenFromHeader = (req) => {
  const authHeader = req.headers.authorization
  if (!authHeader) {
    return null
  }

  const headerParts = authHeader.split(' ')
  if (headerParts.length < 2 || headerParts[0] !== 'Bearer') {
    return null
  }

  return headerParts[1]
}

module.exports = async (req, _res, next) => {
  try {
    const token = getTokenFromHeader(req)
    if (!token) {
      return next('Authorization token not found')
    }

    const user = await User.findById(token)
    if (!user) {
      return next('User not found')
    }

    req.user = user
  } catch (e) {
    return next(e)
  }

  next()
}
