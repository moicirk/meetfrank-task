const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  text: {
    type: String,
    required: true
  },
  sender: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  recipient: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  read: { type: Boolean, default: false }
}, {
  timestamps: true,
  toJSON: {
    transform: (doc, { _id, text, createdAt, sender, recipient }) => {
      return { _id, text, createdAt, sender, recipient }
    }
  }
})

module.exports = mongoose.model('Message', schema)
