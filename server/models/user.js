const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    index: true
  }
}, {
  timestamps: true,
  toJSON: {
    transform: (doc, { _id, username }) => {
      return { _id, username }
    }
  }
})

schema.statics.findWithUnread = function (user) {
  return this.aggregate([
    {
      $lookup: {
        from: 'messages',
        let: { user_id: "$_id" },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$recipient', user._id ] },
                  { $eq: ['$sender', '$$user_id'] },
                  { $eq: ['$read', false] },
                ]
              }
            }
          }
        ],
        as: 'unread'
      }
    },
    {
      $project: {
        _id: 1,
        username: 1,
        unread: { $size: '$unread' }
      }
    },
    { $sort: { unread: -1 } }
  ])
}

module.exports = mongoose.model('User', schema)
