const mongoose = require('mongoose')
const User  = require('./user')
const Message  = require('./message')

mongoose.connect('mongodb://mongo/main', {
  auth: { authSource: 'admin' },
  user: 'root',
  pass: 'example'
})
mongoose.connection.on('error', console.error.bind(console, 'connection error:'))
mongoose.connection.once('open', () => {
  console.log('MongoDB connected')
})

module.exports = { User, Message }
