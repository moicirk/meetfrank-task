const router = require('express').Router()

router.use('/users', require('./users'))
router.use('/chats', require('./chats'))

router.use((err, req, res, next) => {
  console.log('ERROR', err)
  next(err)
})

module.exports = router
