const router = require('express').Router()
const authMiddleware = require('../middlewares/auth')
const { User, Message } = require('../models/index')

const messages = async (req, res, next) => {
  try {
    const me = req.user._id
    const opponent = req.params.user

    await Message.updateMany({ recipient: me }, { read: true })

    const messages = await Message
      .find({
        $or: [
          { $and: [{ sender: me, recipient: opponent }] },
          { $and: [{ sender: opponent, recipient: me }] },
        ]
      })
      .populate('sender recipient')
      .sort({ createdAt: 1 })

    res.json(messages)
  } catch (e) {
    console.error(e)
    next(e)
  }
}

const sendMessage = async (req, res, next) => {
  const { text } = req.body
  const { user } = req.params

  if (!user) {
    return res.status(400).json({ errors: { recipient: 'Recipient required' } })
  }

  if (!text) {
    return res.status(400).json({ errors: { text: 'Message text required' } })
  }

  try {
    const sender = req.user
    const recipient = await User.findById(user)

    const message = await Message.create({
      text,
      sender,
      recipient
    })

    res.json(message)
  } catch (e) {
    console.error(e)
    next(e)
  }
}

router.get('/:user', authMiddleware, messages)
router.post('/:user', authMiddleware, sendMessage)

module.exports = router
