const router = require('express').Router()
const authMiddleware = require('../middlewares/auth')
const { User } = require('../models/index')

const joinUser = async (req, res) => {
  const { username } = req.body
  if (!username) {
    return res.status(400).json({ errors: { username: 'username is required' } })
  }

  try {
    let user = await User.findOne({ username })
    if (!user) {
      user = await User.create({ username })
    }

    res.json({ user })
  } catch (e) {
    console.error(e)
    res.json({ user: null })
  }
}

const getUser = async (req, res) => {
  res.json(req.user)
}

const getUsers = async (req, res) => {
  try {
    const result = await User.findWithUnread(req.user)
    res.json(result)
  } catch (e) {
    console.error(e)
    next(e)
  }
}

router.get('/me', authMiddleware, getUser)
router.get('/', authMiddleware, getUsers)
router.post('/', joinUser)

module.exports = router
