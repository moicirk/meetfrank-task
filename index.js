const { join } = require('path')
const io = require('socket.io')

global.root = __dirname
global.server = join(__dirname, 'server')

const app = require('./server/app')
const port = process.env.PORT || 3000

const server = app.listen(port, () => {
  console.log(`listening on ${port}`)
})

const socket = io(server)
socket.on('connection', (connection) =>{
  console.log('a user is connected', connection.id)

  connection.on('message', (msg) => {
    console.log('Message received', msg)
    connection.broadcast.emit(`message/${msg.recipient._id}`, msg)
  })

  connection.on('newUser', (msg) => {
    connection.broadcast.emit('newUser', msg)
  })
})

