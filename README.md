### MeetFrank test task

All the env variables are already set. This is a bad practice to leave envs 
and compiled code in repository, but it was made for launch simplicity.

Run
```bash
docker-compose up -d
```

Open `http://localhost:3005` in browser (this port chosen because I have a lot of apps running already =)) 

Full application (API, front-end, websocket server) are included in a single
container. Normally, they must be separated in a different services, but I simplified
the run. 
For real-time behavior websocket protocol used.
